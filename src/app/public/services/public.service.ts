import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
/* import data from '../../data-base/data-base.json'
 */
@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private httpClient :HttpClient) { }

login(user:any): Observable<any> {
  console.log(user)
  let params = new HttpParams();
  params = params.append('email', user.email);
  params = params.append('password',user.password);

  const url = `http://localhost:3000/users`
  return this.httpClient.get(url, {params : params})

  }
}
