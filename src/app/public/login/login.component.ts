import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
email : any  ; 
password : any
  constructor(private publicService: PublicService,
    private router :Router) { }

  ngOnInit(): void {
  }
  login() {
    console.log(this.email,this.password)
    const request = {
      email : this.email,
      password : this.password
    }
    this.publicService.login(request).subscribe((user) => {
      if(user?.length> 0) {
        localStorage.setItem('loggedIn' , "true")
        localStorage.setItem('role' , user[0]?.role)

        this.router.navigateByUrl('private')
      }
    
    },
    error => {console.log(error)}
    )
  }
}
