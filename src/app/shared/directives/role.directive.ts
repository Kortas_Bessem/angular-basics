import { Directive, Renderer2, ElementRef } from '@angular/core';

@Directive({
  selector: '[appRole]'
})
export class RoleDirective {


  constructor(
    private elementRef: ElementRef, 
    private renderer: Renderer2) {
      if(localStorage.getItem('role') !="admin") {
    this.renderer.setStyle(this.elementRef.nativeElement, 'display' , 'none' ) 
     }
     }

}
