import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'upperCaseFirstChar'
})
export class UpperCaseFirstCharPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    let first = value.substr(0,1).toUpperCase();
    return first + value.substr(1); 
  }

}
