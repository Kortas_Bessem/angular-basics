import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private httpClient : HttpClient) { }

  getPosts() : Observable<any> {

    return this.httpClient.get("http://localhost:3000/posts")
  
     }

     addPost(post :any) : Observable<any> {

      return this.httpClient.post("http://localhost:3000/posts",post)
     
       }
       updatePost(newPost :any) : Observable<any> {
        const url = `http://localhost:3000/posts/${newPost.id}`;

        return this.httpClient.put(url, newPost)
        
        }
        deletePost(post :any) : Observable<any> {
          const url = `http://localhost:3000/posts/${post}`;
          return this.httpClient.delete(url)

        
          }
}
