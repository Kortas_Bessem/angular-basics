import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient :HttpClient) { }

  getUser() : Observable<any> {
return this.httpClient.get("http://localhost:3000/users/") ;


  }

  getUserByName(username : string , password:string) :Observable<any> {
let params = new HttpParams();
params = params.append('username', username);
params = params.append('password',password);

    return  this.httpClient.get("http://localhost:3000/users/",{params : params}) ;
  }
}
