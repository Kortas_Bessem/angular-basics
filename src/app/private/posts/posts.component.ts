import { Component, OnInit, ViewChild } from '@angular/core';
import { PostService } from '../services/post.service';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
posts : any  ;
postDescription : any ; 
postName : any ; 
postId :any ; 
@ViewChild('closeModal') closeModal : any ;
  constructor(private postService : PostService) { }

  ngOnInit(): void {
   this.getPost()
  }
 
  getPost() {
    this.postService.getPosts().subscribe((posts) => {
      console.log(posts);
      this.posts = posts ;
    }, 
    error => {console.log(error)})
  }
  
  
addPost() {
  const request ={
    "name"  : this.postName,
    "description" : this.postDescription
  }
  this.postService.addPost(request).subscribe((posts) => {
    console.log(posts)
    this.getPost()
  }, 
  error => {console.log(error)})
}

  updatePost(){
    const request ={
    "id": this.postId,
    "name"  : this.postName, 
    "description" : this.postDescription}
this.closeModal.nativeElement.click()
    this.postService.updatePost(request).subscribe((posts) => {
    
      console.log('posts',posts)
      this.getPost()

    },
    error => console.log(error))
  }
  deletePost(post:any){

    this.postService.deletePost(post.id).subscribe((posts) => {
      console.log('posts',posts)
      this.getPost()

    },
    error => console.log(error))
  }

  prepareModal(post:any) {
    this.postName = post.name ;
    this.postDescription = post.description
    this.postId = post.id
  }
  clearModal() {
    this.postName = ''
    this.postDescription = ''
    this.postId = ''
  }
}

