import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users:any ; 
  constructor(private userService: UserService) { }

  ngOnInit(): void {
 this.getUser()
 this.getUserByName('bessem','12345678')

  }
  getUser(){
    this.userService.getUser().subscribe(
      (users:any) => {
        this.users = users ;
        console.log("users", users)
  },
    error => {console.log(error)}
    )
  }

  getUserByName(username:string , password : string ){

    this.userService.getUserByName(username,password).subscribe(
      (user) => {console.log(user)}, 
      error => {console.log(error)}
      )
  }

  
}
