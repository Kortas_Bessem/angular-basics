import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuard } from '../shared/guards/role.guard';
import { HomeComponent } from './home/home.component';
import { PostsComponent } from './posts/posts.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path : 'users' ,canActivate: [RoleGuard], component :UsersComponent},
  {path : 'posts' , component: PostsComponent}, 
  {path: 'home'  , component : HomeComponent},
  {path : '' , redirectTo:'home' , pathMatch:'full'},
  {path : '**' , redirectTo:'home' , pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateRoutingModule { }
