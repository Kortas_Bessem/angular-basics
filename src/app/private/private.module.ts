import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivateRoutingModule } from './private-routing.module';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { PostsComponent } from './posts/posts.component';
import {UpperCaseFirstCharPipe} from '../shared/pipes/upper-case-first-char.pipe'
import { RoleDirective } from '../shared/directives/role.directive';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    HomeComponent,
    UsersComponent,
    PostsComponent,
    UpperCaseFirstCharPipe,
    RoleDirective
    
  ],
  imports: [
    CommonModule,
    PrivateRoutingModule,
    FormsModule
  ]
})
export class PrivateModule { }
